CREATE TABLE tbl_book (
  id SERIAL PRIMARY KEY ,
  title VARCHAR,
  author VARCHAR,
  publisher VARCHAR,
  thumbnail VARCHAR
);


