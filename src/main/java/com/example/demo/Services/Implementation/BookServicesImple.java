package com.example.demo.Services.Implementation;

import com.example.demo.Model.Book;
import com.example.demo.Repository.BookRepository;
import com.example.demo.Services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServicesImple implements BookServices {

    private BookRepository bookRepository;

    @Autowired
    public BookServicesImple(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllServices() { //return values from getAllRepository to this method
        return this.bookRepository.getAllRepository();
    }

    @Override
    public Book findOne(Integer id) {
        return this.bookRepository.findOne(id);
    }

    @Override
    public Boolean update(Book book) {
        return this.bookRepository.update(book);
    }

    @Override
    public Boolean remove(Integer id) {
        return this.bookRepository.remove(id);
    }

    @Override
    public Integer count() {
        return this.bookRepository.count();
    }

    @Override
    public Boolean create(Book book) {
        return this.bookRepository.create(book);
    }
}
