package com.example.demo.Services;

import com.example.demo.Model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
}
