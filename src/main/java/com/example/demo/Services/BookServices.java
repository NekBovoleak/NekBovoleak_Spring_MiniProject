package com.example.demo.Services;


import com.example.demo.Model.Book;

import java.util.List;

public interface BookServices {
    List<Book> getAllServices();

    Book findOne(Integer id);

    Boolean update(Book book);

    Boolean remove(Integer id);

    Integer count();

    Boolean create(Book book);
}
