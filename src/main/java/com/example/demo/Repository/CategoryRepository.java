package com.example.demo.Repository;

import com.example.demo.Model.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("select * from tbl_category order by id")
    List<Category> getAll();
}
