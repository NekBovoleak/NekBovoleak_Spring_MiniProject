package com.example.demo.Repository;


import com.example.demo.Model.Book;
import com.example.demo.Repository.providers.BookProvider;
import com.github.javafaker.Faker;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface BookRepository {

//    @Select("select * from tbl_book order by id")
    @SelectProvider(type = BookProvider.class, method = "getAllProvider")
    @Results({
             @Result(column = "id", property="id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> getAllRepository();

    @Select("select * from tbl_book b INNER JOIN tbl_category c ON b.cate_id = c.id where b.id=#{id}")
    @Results({
            @Result(column = "id", property="id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    Book findOne(@Param("id") Integer id);

    @Update("update tbl_book set title=#{title}, author=#{author}, publisher=#{publisher}, thumbnail=#{thumbnail}, cate_id=#{category.id} where id=#{id}")
    boolean update(Book book);

    @Delete("delete from tbl_book where id=#{id}")
    boolean remove(Integer id);

//    @Insert("insert into tbl_book(title,author,publisher,thumbnail) values(#{title},#{author},#{publisher},#{thumbnail})")
    @InsertProvider(type = BookProvider.class, method = "createProvider")
    boolean create(Book book);


//    Faker faker = new Faker();
//    List<Book> bookList = new ArrayList<>();
//
//    {
//        for (int i = 1; i < 11; i++) {
//            Book book = new Book();
//            book.setId(i);
//            book.setTitle(faker.book().title());
//            book.setAuthor(faker.book().author());
//            book.setPublisher(faker.book().publisher());
//
//            bookList.add(book);
//        }
//    }
    //return values (id,title,author,publisher) to this method

//    public List<Book> getAllRepository() {
//        return this.bookList;
//    }
//
//    public Book findOne(Integer id) {
//        for (int i = 0; i < bookList.size(); i++) {
//            if (bookList.get(i).getId() == id)
//                return bookList.get(i);
//        }
//        return null;
//    }
//
//    public boolean update(Book book) {
//        for (int i = 0; i < bookList.size(); i++) {
//            if (bookList.get(i).getId() == book.getId()) {
//                bookList.set(i, book);
//                return true;
//            }
//
//        }
//        return false;
//    }
//
//    public boolean remove(Integer id){
//        for(int i=0;i<bookList.size();i++){
//            if(bookList.get(i).getId()==id){
//                bookList.remove(i);
//                return true;
//            }
//        }
//        return false;
//    }
//
    public int count();
//        return bookList.size();

//
//    public boolean create(Book book){
//        return bookList.add(book);
//    }
}
