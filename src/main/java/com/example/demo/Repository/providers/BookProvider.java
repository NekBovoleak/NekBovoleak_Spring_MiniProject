package com.example.demo.Repository.providers;

import com.example.demo.Model.Book;
import org.apache.ibatis.jdbc.SQL;
import sun.awt.SunHints;

public class BookProvider {
    public String getAllProvider(){
        return new SQL(){{
            SELECT ("*");
            FROM("tbl_book b");
            INNER_JOIN("tbl_category c ON b.cate_id = c.id");
            ORDER_BY("b.id");
        }}.toString();
    }

    public String createProvider(Book book){
        return new SQL(){{
            INSERT_INTO("tbl_book");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("publisher","#{publisher}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("cate_id","#{category.id}");
        }}.toString();
    }
}
