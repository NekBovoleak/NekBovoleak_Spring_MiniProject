package com.example.demo.Controllers;


import com.example.demo.Model.Book;
import com.example.demo.Model.Category;
import com.example.demo.Services.BookServices;
import com.example.demo.Services.CategoryService;
import com.example.demo.Services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.rmi.server.InactiveGroupException;

import javax.jws.WebParam;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class BookControllers {
    private BookServices bookServices;
    private UploadService uploadService;
    private CategoryService categoryService;

    public BookControllers(BookServices bookServices, UploadService uploadService, CategoryService categoryService) {
        this.bookServices = bookServices;
        this.uploadService = uploadService;
        this.categoryService=categoryService;
    }

    //    @GetMapping({"/index","/","/home"})
    @RequestMapping(method = RequestMethod.GET,value = {"/index","/","/home"})
    public String index(Model model){
        List<Book> bookList = this.bookServices.getAllServices();
        model.addAttribute("books",bookList);
        return "Book/index";
    }

    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id")Integer id,Model model){
        System.out.println("ID : "+id);
        Book book = this.bookServices.findOne(id);
        model.addAttribute("book",book);
        return "Book/view-detail";
    }

    @GetMapping("/update/{id}")
    public String updateBook(@PathVariable Integer id, ModelMap modelMap){
        Book book = this.bookServices.findOne(id);
        modelMap.addAttribute("isNew",false);
        modelMap.addAttribute("book",book);
        List<Category> categories = this.categoryService.getAll();
        modelMap.addAttribute("categories", categories);
        return "Book/create-book";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file){


        File path = new File("/pp/");
        if(!path.exists())
            path.mkdirs();

        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.')+1);
        filename = UUID.randomUUID() + "." +extension;
        try{
            Files.copy(file.getInputStream(),Paths.get("/pp/", filename));
        }catch (Exception e){

        }
        if(!file.isEmpty()){
            book.setThumbnail("/images-pp/"+filename);
        }
        this.bookServices.update(book);
        return "redirect:/index";
    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id){
        this.bookServices.remove(id);
        return "redirect:/";
    }

    @GetMapping("/count")
    @ResponseBody
    public Map<String, Object> count(){
        Map<String, Object> response = new HashMap<>();
        response.put("record_count", this.bookServices.count());
        response.put("status",true);
        return response;
    }

    @GetMapping("/create")
    public String create(Model model){
        model.addAttribute("book", new Book());
        model.addAttribute("isNew",true);
        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("categories", categories);
        return "Book/create-book";
    }

    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file, Model model){
        if(bindingResult.hasErrors()){
            model.addAttribute("isNew",true);
            return "Book/create-book";
        }
        String filename = this.uploadService.upload(file,"pp/");
        System.out.println(filename);
        book.setThumbnail("/images-pp/"+filename);
        this.bookServices.create(book);
        return "redirect:/index";
    }

    @GetMapping("/multiple-file-upload")
    public String MultipleUpload(){
        return "Book/multiple-file-upload";
    }

    @PostMapping("/multiple-file-upload/submit")
    public  String uploadSubmit(@RequestParam("file") List<MultipartFile>files){
        if(files.isEmpty()){
            System.out.println("Nothing");
        }
        files.forEach(file -> {
            System.out.println(file.getOriginalFilename());
        });

        return "";
    }

}
